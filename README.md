# Sample Microservices app on OKE for "OCI CCST"

## Abstract

This tutorial walks you through the creation of a Kubernetes Cluster on Oracle Cloud Infrastructure or OKE (Oracle Kubernetes Engine) via Terraform & Oracle Resource Manager, and the deployment of a sample micro-service application on the cluster. Some basic Kubernetes management activities are also described in details.

The four Hands-On Labs (a.k.a. HOL) included in this tutorial deal with:

* HOL1: OKE creation with Terraform & Resource Manager
* HOL2: sample micro-services sample application deployment
* HOL3: some load gets injected to the cluster and the consoles are used to show how it reacts. The cluster is stressed and the Horizontal Pod Autoscaler functionality is shown live.
* HOL4: rolling update / rollback & protecting from errors

The following tools are also installed automatically as part of the HOL1:

* EFK Stack (ElasticSearch + Fluentd + Kibana)
* Grafana & Prometheus
* Helm

Helm tool is used to deploy the micro-service application on Kubernetes.

The full tutorial execution takes about one hour.

## Prerequisite

### Information to be gathered

The attendee needs to gather the following information from the tenant to be used in the Resource Manager:

* compartment_id = "ocid1.compartment.oc1..xyzxyzxyzxyzxyzxyzxyzxyzxyz"
* tenancy_id = "ocid1.tenancy.oc1..xyzxyzxyzxyzxyzxyzxyzxyzxyz"
* user_id = "ocid1.user.oc1..xyzxyzxyzxyzxyz"

### Service limit check

The following OCI resources will be deployed (a.k.a. BOM) on OCI by Terraform described in HOL1.

* n. 1 VCN + Security Lists + Routing Roles + Internet Gateway + Service Gateway

* n. 1 OKE Cluster containing n. 2 node pool with a total of 6 VM.Standard2.2 (*) + relative standard boot volume storage for each VM

* n. 2 VM.Standard.E2.1 (Admin and Bastion)
* n. 5 LBaaS with Shape 100Mbps

The user must check if her/his service limits allows to create the BOM.

As an OKE cluster will be created, we assume the OKE [pre-requisites](https://docs.cloud.oracle.com/en-us/iaas/Content/ContEng/Concepts/contengpolicyconfig.htm#PolicyPrerequisitesGroups) have been already done.

(*) the node pool VM shape can be edited changing the zip content directly in the file "variables.tf" (look for "node_pools" string). Bear in mind that this might led to misbehaviour as this change hasn't been tested.

## Main disclaimer

The OCI infrastructure created by using Terraform zip provided (ref. "sample-microservices-terraform-oci-oke.zip") is for testing purposes only and, for several reasons, it is not advisable to use it in real scenarios.

Moreover I warn you against using it on any Oracle client context e.g. production environment.

## HOL1: OKE creation with Terraform and Resource Manager

### Prerequisite

Make sure all prerequisites to run Stacks and Jobs described [here](https://docs.cloud.oracle.com/en-us/iaas/Content/ResourceManager/Tasks/managingstacksandjobs.htm#prerequisites) are satisfied.

#### Uploading API Keys


Add the following public key [oci_api_key_champion_public.pem](./collaterals/oci_api_key_champion_public.pem) into your user API Keys. More info at the step 13 [here](https://docs.cloud.oracle.com/en-us/iaas/Content/Functions/Tasks/functionssetupapikey.htm).

When the key is uploaded the following fingerprint should be displayed (w/o quotes):

"4d:8e:1c:0c:11:c7:0a:50:55:fc:17:90:57:fe:58:e2"
> With the new Resource manager version this step might be not necessary any more. Considering that this zip file was created months ago I suggest to do it.



### Running the Resource Manager

Log-in the OCI console using your user.

Follow the basic Resource Manager instruction to create a new stack:

* Choose: Resource Manager / Stacks
* Create a New Stack.
* Download on your local hard-disk the zip file provided [sample-microservices-terraform-oci-oke.zip](./collaterals/sample-microservices-terraform-oci-oke.zip) from collaterals folder.
* Upload the zip above in the area "SELECT A TERRAFORM CONFIGURATION (.ZIP) FILE TO UPLOAD"
* Give an arbitrary name and description
* Select the compartment
* TERRAFORM VERSION filed should be already configured to 0.12.x
* Fill-in the correct values (as gathered in the prerequisite paragraph above) for the following parameters:

![image-20200329225841226](images/image-20200329225841226.png)



* Press "Next" and then "Create"

Using the "Stack Details" page, launch the "Plan" from the "Terraform Action" combo. Give a name to the Plan if necessary and then press Plan button.

Check out	 the Plan log, the output at the very end of the page should be:

`Plan: 28 to add, 0 to change, 0 to destroy.`

If everything ok, then get back to the "Stack Details" page and proceed with the "Apply" from "Terraform Action" combo.

Give a name to the Apply job, if necessary, and then press Apply button (you can select auto-approve or the Plan just created).

Now the Apply job should be "In progress" state. It should take 10 to 15 minutes to complete and create all the resources.

Click the Apply Job to see logs (from where get info about admin host public IP)

> Beware that besides the OKE infrastructure and helper hosts (bastion and admin), the Terraform will deploy the following components:
>
>* EFK stack
>* Grafana / Prometheus
>* Helm
>* Metric Server
>* Kube-View
>* Helm
>* k9s
>* nfs-server-provisioner (to simplify the PV configuration of ES)

### Connection to admin host via SSH 

When the Apply job finishes, it displays in the log at the very of the page the admin host public. Look for `admin_public_ip`.

Then use the SSH private keys download from the [collaterals](./collaterals) folder to connect to admin host via SSH using `opc` user.

> Bear in mind that SSH connection to OCI hosts doesn't work with Oracle VPN active

### Gather the tools URL and credentials

Once connected via SSH on the admin host (via public IP) run ./tools.sh to show the tools connection URLs and credentials.

```bash
./tools.sh
```

> If you get weird characters then try to check whether the LANG env variable is correctly set to: en_US.UTF-8. If not run this command before running tools shell.
>
> export LANG=en_US.UTF-8

### EFK configuration

Kibana and ElasticSearch need basic index configuration.

> Please consider that the SSL certificate is self-signed therefore you want to accept all the SSL warning in the browser

Once logged in the Kibana UI using the information provided above via `tools.sh` script, follow the instruction below:

* Press first: "Explore on my own"

* Create a new index pattern directly from "Discovery" menu. Alternatively use Management \ Index patterns \ Create index pattern (close the pop-up that gets shown using the "x" sign on the top-right corner).

* Insert: `log*` in the "Index pattern" input box then "Next step" button.

* Select: `@timestamp` from "Time Filter field name" combo box then "Create index pattern" button.

Now you are able to browse collected logs from "Discovery" menu.

## HOL2: sample microservices sample application deployment

Login on terraform-oke admin host via SSH.

### Install the microservices sample application

```bash
cd ~/sample-microservices-app/handson_2/

helm install --name sample-app k8s_sample-microservices-app/ --values k8s_sample-microservices-app/values.yaml --namespace sample-app

```

During installation check the following:

```bash
watch -n 1 kubectl get po -n core-services -o wide
kubectl get svc -n main-app
```
After the application is fully installed you can get the public application IP with the following commands:
```bash
export SAMPLE_APP_SERVICE_IP=$(kubectl get svc --namespace main-app frontend-ui-service -o jsonpath='{.status.loadBalancer.ingress[0].ip}')

echo $SAMPLE_APP_SERVICE_IP
```
<br/>
The app can be tested using the following command or via Browser:

```bash
curl "http://$SAMPLE_APP_SERVICE_IP/LoadOKE/TestOKEService?servlist=email-service.core-services.svc.cluster.local:8080,pdf-generation-service.core-services.svc.cluster.local:8080,digitalsignchecker-service.core-services.svc.cluster.local:8080&threadnum=5,5,5&elabtime=100,100,100&errperc=10,5,10"
```

###HOL2.1 Updating sample application deployment params via helm  (optional)
The values used for the installation can be modified here:

```bash
vim k8s_sample-microservices-app/values.yaml
```

For instance you can change the following parameters:
```yaml
pdfGeneration:
...
  horizontalPodAutoscaler:
    spec:
      minReplicas: 5
      maxReplicas: 20
```
Check the pdfGeneration `horizontalPodAutoscaler` before applying the change to the cluster:

```bash
kubectl get horizontalPodAutoscaler -n core-services
```

As you can see all the horizontalPodAutoscaler (HPA) in the core-services has the same value therefore the same ability to scale.

To check the HPA size also look at KubeView (choose the name space named "core-services" in the input box at the top). If you opened KubeView before installing sample application, then you need to refresh the whole page (F5 on Windows or Control-R on Mac) as the namespace "core-services" is not loaded.

Then deploy an update using the following helm command:

```bash
helm upgrade sample-app k8s_sample-microservices-app/ --namespace sample-app --values k8s_sample-microservices-app/values.yaml
```

Check again:

```bash
kubectl get horizontalPodAutoscaler -n core-services
```



## HOL3: Siege the cluster and showing the console

Login on terraform-oke admin host via SSH.

Before start the siege check how is the system when in idle state via:

* Kibana UI

* Grafana UI

* KubeView (choose the name space named "core-services" in the input box at the top). If you opened KubeView before installing sample application, then you need to refresh the whole page (F5 on Windows or Control-R on Mac) as the namespace "core-services" is not loaded.

Run the following command to inject some load in the custom sample application.

```bash
export SERVICE_IP=$(kubectl get svc --namespace main-app frontend-ui-service -o jsonpath='{.status.loadBalancer.ingress[0].ip}')

echo $SERVICE_IP

siege -c 10 -r 10000 "http://$SERVICE_IP/LoadOKE/TestOKEService?servlist=email-service.core-services.svc.cluster.local:8080,pdf-generation-service.core-services.svc.cluster.local:8080,digitalsignchecker-service.core-services.svc.cluster.local:8080&threadnum=5,5,5&elabtime=100,100,100&errperc=10,5,10"
```

Using your browser:

* Check logs collected by EFK stack

* Check the computational resources consumed via Grafana (e.g. using dashboard "Kubernetes / Compute Resources / Cluster")

* Using other SSH connection check the cluster size with following commands:

```bash
watch -n 1 "kubectl top no && kubectl get hpa,po -n core-services"

kubectl get po,svc -n main-app
watch -n 1 kubectl top po -n core-services
watch kubectl get hpa,po,svc -n core-services
```

Once the pod deployment reaches its maximum size (HPA is set to max 10 pod per microservice), you can stop the siege with Control+C.

## HOL4: rolling update / rollback & protecting from errors

### Rolling update / rollback

Login on terraform-oke admin host via SSH.

<br/>

Install`Generic-Microservice` pod installed in version 1 and the relative service with type: LoadBalancer

```bash
cd ~/sample-microservices-app/handson_4

kubectl get po,svc

kubectl apply -f generic-microservice-nochecks.yaml

kubectl get po,svc
```
Test the generic-microservice deployment:
```bash
export GENERIC_MICRO_SERVICE_IP=$(kubectl get svc --namespace default generic-microservice -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
echo $GENERIC_MICRO_SERVICE_IP

while true; do curl http://$GENERIC_MICRO_SERVICE_IP; sleep 0.5;done
```

Keep this loop running, this will show the deployment. Verify the service balances the requests on all the pods (the host name changes).

Open a new SSH shell and for instance scale manually the pod:

```bash
kubectl scale deployment --replicas=10 generic-microservice
```

Check the new deployment status in the cluster:

```bash
kubectl get po,svc
```

Now let's update the application version using a rolling update strategy:

```bash
kubectl set image deploy generic-microservice app=rmagnani/kubeserve:v2 --record
```

Check the status:

```bash
kubectl rollout history deployment generic-microservice
```

Verify the version update on the other shell where is the `curl` loop still active.

Now let's update the application version to a **WRONG** application version:

```bash
kubectl set image deploy generic-microservice app=rmagnani/kubeserve:error --record
```

Rollback to a previous version:

```bash
kubectl rollout undo deployment generic-microservice
```

Again verify the version update and response HTTP status on the other shell where is the `curl` loop still active.

Now the application is not working and the final user are experiencing some outage.

### Exploit probing

Keep the shell with `curl` loop still active.

Apply `generic-microservice-checks.yaml`:

```bash
cd ~/sample-microservices-app/handson_4

kubectl apply -f generic-microservice-checks.yaml
```

Now the application is back to version v2 but now we added the Readiness and Liveness probes.

Edit deployment and set again the image with *error* label (that have inside some code errors):

```bash
kubectl set image deploy generic-microservice app=rmagnani/kubeserve:error --record
```

Now the error application never gets live.

Again verify what happened on the testing application using the other shell where is the `curl` loop still active.

## Destroying the cluster

To destroy the infrastructure use Resource Manager Terraform Action / Destroy.

> Unfortunately the LoadBalancer created by Kubernetes don't get deleted (because not created directly by Terraform). This might lead to Destroy action to fail.
>
> Remove LoadBalancer manually then later on get rid of the VCN as well (that is where Resource Manager failed to destroy).

## About the authors

```
Riccardo Magnani
Technology Architect Director for Oracle Consulting
Italy
```

```
Francesco Pacilio
Technology Architect for Oracle Consulting
Italy
```

